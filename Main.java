package com.company;

public class Main {

    public static void main(String[] args) {

        Car model1 = new Car("Honda");
        Car model2 = new Car("Toyota");
        Car model3 = new Car("Subaru");
        Car model4 = new Car("Tesla");

        CarSales carModels = new CarSales();
        carModels.add(model1);
        carModels.add(model2);
        carModels.add(model3);
        carModels.add(model4);

        String carDealershipSales = carModels.toString();

        System.out.println("Welcome to the dealership! Look at our products!");
        System.out.println("Cars: " + carDealershipSales);
    }


}
