package com.company;

public class CarSales extends Main {
    Car[] carModels;
    private Car[] dealerList = new Car[10];

    public CarSales() {
        this.carModels = new Car[10];
    }

    public String toString() {
        String list = dealerList[0].getModel();
        for(int i = 1; i < dealerList.length; i++) {
            if(dealerList[i] == null) {
                return list;
            }

            list = list + ", " + dealerList[i].getModel();
        }
        return list;
    }

    public void add(Car product) {
        int count = 0;
        if (count == dealerList.length) {
            throw new IndexOutOfBoundsException();
        }
        dealerList[count] = product;
        count++;
    }
}
